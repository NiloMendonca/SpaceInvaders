package jogoep2;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SpriteTest {
    
    public SpriteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLoadImage() {
        System.out.println("loadImage");
        String imageName = "";
        Sprite instance = null;
        instance.loadImage(imageName);
    }

    @Test
    public void testGetImageDimensions() {
        System.out.println("getImageDimensions");
        Sprite instance = null;
        instance.getImageDimensions();
    }

    @Test
    public void testGetImage() {
        System.out.println("getImage");
        Sprite instance = null;
        Image expResult = null;
        Image result = instance.getImage();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetX() {
        System.out.println("getX");
        Sprite instance = null;
        int expResult = 0;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetY() {
        System.out.println("getY");
        Sprite instance = null;
        int expResult = 0;
        int result = instance.getY();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetWidth() {
        System.out.println("getWidth");
        Sprite instance = null;
        int expResult = 0;
        int result = instance.getWidth();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        Sprite instance = null;
        int expResult = 0;
        int result = instance.getHeight();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsVisible() {
        System.out.println("isVisible");
        Sprite instance = null;
        boolean expResult = false;
        boolean result = instance.isVisible();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetVisible() {
        System.out.println("setVisible");
        Boolean enable = null;
        Sprite instance = null;
        instance.setVisible(enable);
    }

    @Test
    public void testGetBounds() {
        System.out.println("getBounds");
        Sprite instance = null;
        Rectangle expResult = null;
        Rectangle result = instance.getBounds();
        assertEquals(expResult, result);
    }

    @Test
    public void testPaintComponent() {
        System.out.println("paintComponent");
        Graphics g = null;
        Sprite instance = null;
        instance.paintComponent(g);
    }

    public class SpriteImpl extends Sprite {

        public SpriteImpl() {
            super(0, 0);
        }
    }
    
}
