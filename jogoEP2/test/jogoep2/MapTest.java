package jogoep2;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MapTest {
    
    public MapTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testPaintComponent() {
        System.out.println("paintComponent");
        Graphics g = null;
        Map instance = new Map();
        instance.paintComponent(g);
    }

    @Test
    public void testDesenhaLose() {
        System.out.println("desenhaLose");
        Graphics g = null;
        Map instance = new Map();
        instance.desenhaLose(g);
    }

    @Test
    public void testDesenhaWin() {
        System.out.println("desenhaWin");
        Graphics g = null;
        Map instance = new Map();
        instance.desenhaWin(g);
    }

    @Test
    public void testFuncaoDesenhaInimigos() {
        System.out.println("funcaoDesenhaInimigos");
        int nivel = 0;
        Map instance = new Map();
        instance.funcaoDesenhaInimigos(nivel);
    }

    @Test
    public void testActionPerformed() {
        System.out.println("actionPerformed");
        ActionEvent e = null;
        Map instance = new Map();
        instance.actionPerformed(e);
    }

    @Test
    public void testRun() {
        System.out.println("run");
        Map instance = new Map();
        instance.run();
    }

    @Test
    public void testRetiraVidas() {
        System.out.println("retiraVidas");
        Map instance = new Map();
        instance.retiraVidas();
    }

    @Test
    public void testPosicionaVetor() {
        System.out.println("posicionaVetor");
        Map instance = new Map();
        int expResult = 0;
        int result = instance.posicionaVetor();
        assertEquals(expResult, result);
    }

    @Test
    public void testPosicaoInimigoY() {
        System.out.println("posicaoInimigoY");
        Map instance = new Map();
        int expResult = 0;
        int result = instance.posicaoInimigoY();
        assertEquals(expResult, result);
    }

    @Test
    public void testPosicaoInimigoX() {
        System.out.println("posicaoInimigoX");
        Map instance = new Map();
        int expResult = 0;
        int result = instance.posicaoInimigoX();
        assertEquals(expResult, result);
    }

    @Test
    public void testDesenhaPontos() {
        System.out.println("desenhaPontos");
        Graphics g = null;
        Map instance = new Map();
        instance.desenhaPontos(g);
    }

    @Test
    public void testDesenhaVidas() {
        System.out.println("desenhaVidas");
        Graphics g = null;
        Map instance = new Map();
        instance.desenhaVidas(g);
    }

    @Test
    public void testChama() {
        System.out.println("chama");
        Map instance = new Map();
        instance.chama();
    }

    @Test
    public void testColider() {
        System.out.println("colider");
        int posMissilX = 0;
        int posMissilY = 0;
        Map instance = new Map();
        boolean expResult = false;
        boolean result = instance.colider(posMissilX, posMissilY);
        assertEquals(expResult, result);
    }
    
}
