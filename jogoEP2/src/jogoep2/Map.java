package jogoep2;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener, Runnable {

    //private JFrame frameJogo;
    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;

    private final Image background;
    private final Spaceship spaceship;
    private final Image imageM;
    private final Image imageI;
    private final Image imageII;
    private final Image imageIII;

    private boolean desenhaInimigos = true;
    private boolean fimLoop = false;
    private int FUNCAO_THREAD = 0;
    private int contadorInimigosEasy = 0;
    private int contadorInimigosMedium = 0;
    private int contadorInimigosHard = 0;
    private int FASE = 1;

    private final int qtdInimigos = 30;
    private final int velocidadeInimigo = 1;
    private int posicaoInimigosX = 20;
    private int posicaoInimigosY = 0;
    private final int distanciaEntreInimigosX = 110;
    private final int distanciaEntreInimigosY = 90;
    private int posicaoVetor = 0;
    private final int[] mapaPosicoesInimigosX = new int[qtdInimigos + 2];
    private final int[] mapaPosicoesInimigosY = new int[qtdInimigos + 2];
    private final int[] mapaPosicoesInimigosDestruidos = new int[qtdInimigos + 2];
    private int pontuacaoMaxima = 400;

    private int pontos = 0;
    private int vidas = 10;
    private int suavisa = 0;

    Thread t;

    public Map() {

        addKeyListener(new KeyListerner());

        setDoubleBuffered(true);
        setFocusable(true);
        setRequestFocusEnabled(true);
        setVerifyInputWhenFocusTarget(true);
        setIgnoreRepaint(true);

        //tela de fundo do jogo
        ImageIcon image = new ImageIcon("src/jogoep2/img/space.jpg");
        this.background = image.getImage();
        
        ImageIcon image1 = new ImageIcon("src/jogoep2/img/missile.png");
        this.imageM = image1.getImage();
        
        ImageIcon image2 = new ImageIcon("src/jogoep2/img/alien_EASY.png");
        this.imageI = image2.getImage();
        
        ImageIcon image3 = new ImageIcon("src/jogoep2/img/alien_MEDIUM.png");
        this.imageII = image3.getImage();
        
        ImageIcon image4 = new ImageIcon("src/jogoep2/img/alien_HARD.png");
        this.imageIII = image4.getImage();

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);

            if (vidas >= 0 && pontos <= pontuacaoMaxima) {
                draw(g);
                desenhaPontos(g);
                desenhaVidas(g);

                if (contadorInimigosHard < 2 && contadorInimigosMedium < 2 && FASE == 1) {

                    for (int j = 0; j <= qtdInimigos; j++) {
                        mapaPosicoesInimigosX[j] = -1;
                        mapaPosicoesInimigosY[j] = -1;
                        mapaPosicoesInimigosDestruidos[j] = 0;
                    }

                    posicaoVetor = 0;
                    funcaoDesenhaInimigos(1);
                    FASE = 2;
                } else if (contadorInimigosHard < 2 && contadorInimigosEasy < 2 && FASE == 2) {
                    for (int j = 0; j <= qtdInimigos; j++) {
                        mapaPosicoesInimigosX[j] = -1;
                        mapaPosicoesInimigosY[j] = -1;
                        mapaPosicoesInimigosDestruidos[j] = 0;
                    }

                    posicaoVetor = 0;
                    funcaoDesenhaInimigos(2);
                    FASE = 3;
                } else if (contadorInimigosEasy < 2 && contadorInimigosMedium < 2 && FASE == 3) {
                    for (int j = 0; j <= qtdInimigos; j++) {
                        mapaPosicoesInimigosX[j] = -1;
                        mapaPosicoesInimigosY[j] = -1;
                        mapaPosicoesInimigosDestruidos[j] = 0;
                    }

                    posicaoVetor = 0;
                    funcaoDesenhaInimigos(3);
                    FASE = 1;
                }

                if ((contadorInimigosHard > 1 || contadorInimigosMedium > 1 || contadorInimigosEasy > 1) && fimLoop) {
                    contadorInimigosHard = 0;
                    contadorInimigosMedium = 0;
                    contadorInimigosEasy = 0;
                }
            }
        
        if (pontos > pontuacaoMaxima) {
            suavisa++;
            if (suavisa > 1) {

                desenhaWin(g);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (vidas < 0) {
            suavisa++;
            if (suavisa > 1) {

                desenhaLose(g);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
    }

    public void desenhaLose(Graphics g) {

        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }

        String message = "Voce perdeu!!! :/";
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        
        String message2 = "Pressione ESC para sair";
        Font font2 = new Font("Helvetica", Font.BOLD, 15);
        FontMetrics metric2 = getFontMetrics(font2);
        
        g.setColor(Color.white);
        g.setFont(font2);
        g.drawString(message2, (Game.getWidth() - metric2.stringWidth(message2)) / 2, Game.getHeight() / 2 + 100);
    }

    public void desenhaWin(Graphics g) {

        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }

        String message = "Parabens,\nvoce GANHOU!!! :)";
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics metric = getFontMetrics(font);
               
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        
        String message2 = "Pressione ESC para sair";
        Font font2 = new Font("Helvetica", Font.BOLD, 15);
        FontMetrics metric2 = getFontMetrics(font2);
        
        g.setColor(Color.white);
        g.setFont(font2);
        g.drawString(message2, (Game.getWidth() - metric2.stringWidth(message2)) / 2, Game.getHeight() / 2 + 100);
        
    }

    public void funcaoDesenhaInimigos(int nivel) {

        fimLoop = false;

        //contador de inimigos
        int x = 0;
        desenhaInimigos = true;

        while (desenhaInimigos) {

            if (nivel == 1) {
                FUNCAO_THREAD = 2;
            }

            if (nivel == 2) {
                FUNCAO_THREAD = 3;
            }

            if (nivel == 3) {
                FUNCAO_THREAD = 4;
            }

            chama();
            x++;
            if (x >= qtdInimigos) {
                desenhaInimigos = false;
            }
        }

    }

    private void draw(Graphics g) {

        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        updateSpaceship();
        repaint();
    }

    private void updateSpaceship() {

        spaceship.move();
    }

    @Override
    public void run() {

        Image img = null;
        Graphics g;
        int divisorVelocidadeY = 0, divVelEasy = 13, divVelMedium = 8, divVelHard = 3, divGeral = 0, funcaoThreadLocal = FUNCAO_THREAD;
        boolean primeiroLoop = true;

        if (funcaoThreadLocal == 1) {
            
            img = this.imageM;

            int posicaoMissilY = spaceship.getPosicaoY();
            int posicaoMissilX = spaceship.getPosicaoX();

            boolean stop = false;

            while (posicaoMissilY >= 0 && !stop && vidas >= 0 && pontos <= pontuacaoMaxima) {
                g = this.getGraphics();
                g.drawImage(img, posicaoMissilX + 5, posicaoMissilY, this);

                stop = colider(posicaoMissilX + 5, posicaoMissilY);

                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    System.out.println("Erro no sleep da Thread missil!:(");
                }

                g.dispose();
                posicaoMissilY--;

                FUNCAO_THREAD = 0;
            }
        }

        if (funcaoThreadLocal == 2 || funcaoThreadLocal == 3 || funcaoThreadLocal == 4) {
            
            if (funcaoThreadLocal == 2) {

                contadorInimigosEasy++;
                img = this.imageI;
                divisorVelocidadeY = 0;
                divGeral = divVelEasy;
            }

            if (funcaoThreadLocal == 3) {

                contadorInimigosMedium++;
                img = this.imageII;
                divisorVelocidadeY = 0;
                divGeral = divVelMedium;
            }

            if (funcaoThreadLocal == 4) {

                contadorInimigosHard++;
                img = this.imageIII;
                divisorVelocidadeY = 0;
                divGeral = divVelHard;
            }

            int posicaoVetorLocal = posicionaVetor();

            int novaPosicaoInimigoY = posicaoInimigoY();
            int novaPosicaoInimigoX = posicaoInimigoX();
            while (novaPosicaoInimigoY <= Game.getHeight() && vidas >= 0 && pontos <= pontuacaoMaxima) {
                g = this.getGraphics();
                g.drawImage(img, novaPosicaoInimigoX, novaPosicaoInimigoY, this);

                try {
                    Thread.sleep(5);
                } catch (InterruptedException ex) {
                    System.out.println("Erro no sleep da Thread inimigos!:(");
                }

                g.dispose();

                if (divisorVelocidadeY > divGeral) {
                    novaPosicaoInimigoY += velocidadeInimigo;
                    divisorVelocidadeY = 0;
                }

                divisorVelocidadeY++;

                if (mapaPosicoesInimigosDestruidos[posicaoVetorLocal] == 1) {
                    pontos += 3;
                    break;
                }

                primeiroLoop = false;

                mapaPosicoesInimigosX[posicaoVetorLocal] = novaPosicaoInimigoX;
                mapaPosicoesInimigosY[posicaoVetorLocal] = novaPosicaoInimigoY;
            }
            
            if (novaPosicaoInimigoY >= (Game.getHeight()-3)) {
                    retiraVidas();
            }

            if (funcaoThreadLocal == 2) {
                contadorInimigosEasy--;
            }

            if (funcaoThreadLocal == 3) {
                contadorInimigosMedium--;
            }

            if (funcaoThreadLocal == 4) {
                contadorInimigosHard--;
            }
        }

        FUNCAO_THREAD = 0;
    }

    public synchronized void retiraVidas(){
        vidas--;
    }
    
    public synchronized int posicionaVetor() {
        int posicaoVetorLocal = posicaoVetor;
        posicaoVetor++;
        return posicaoVetorLocal;
    }

    private int resetY = 0;

    public synchronized int posicaoInimigoY() {
        if (posicaoInimigosX > Game.getWidth()) {
            posicaoInimigosY -= distanciaEntreInimigosY;
        }

        if (resetY >= qtdInimigos) {
            posicaoInimigosY = 0;
            resetY = 0;
        }

        resetY++;

        int novaPosicaoInimigoY = posicaoInimigosY;
        return novaPosicaoInimigoY;
    }

    public synchronized int posicaoInimigoX() {
        if (posicaoInimigosX > Game.getWidth()) {
            posicaoInimigosX = 20;
        }

        int novaPosicaoInimigoX = posicaoInimigosX;
        posicaoInimigosX += distanciaEntreInimigosX;
        return novaPosicaoInimigoX;
    }

    public void desenhaPontos(Graphics g) {

        String message = "Pontuacao: " + pontos;
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2 - 10, Game.getHeight() - 7);
    }

    public void desenhaVidas(Graphics g) {
        String message = "Vidas: " + vidas;
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);
        
        if(vidas > 3){
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, 10, Game.getHeight() - 7);
        } else{
            g.setColor(Color.red);
            g.setFont(font);
            g.drawString(message, 10, Game.getHeight() - 7);
        }
    }

    public void chama() {

        t = new Thread(this);
        t.start();
    }

    public boolean colider(int posMissilX, int posMissilY) {

        boolean colidiu = false;
        int dimInimigo = 20, dimMissilWidth = 7, dimMissilHeight = 20;
        Rectangle rectInimigo;
        Rectangle rectMissil;

        for (int j = 0; j <= qtdInimigos; j++) {

            if (mapaPosicoesInimigosDestruidos[j] == 0) {
                rectInimigo = new Rectangle(mapaPosicoesInimigosX[j], mapaPosicoesInimigosY[j], dimInimigo, dimInimigo);
                rectMissil = new Rectangle(posMissilX, posMissilY, dimMissilWidth, dimMissilHeight);

                if (rectInimigo.intersects(rectMissil)) {
                    colidiu = true;
                    mapaPosicoesInimigosDestruidos[j] = 1;
                    break;
                }

            }
        }

        return colidiu;
    }

    private class KeyListerner extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            spaceship.keyPressed(e);
            
            int key = e.getKeyCode();
            if (key == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);

            int key = e.getKeyCode();
            if (key == KeyEvent.VK_SPACE && vidas >= 0 && pontos <= 600) {
                FUNCAO_THREAD = 1;
                chama();
            }
        }
    }
}
