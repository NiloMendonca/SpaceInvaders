package jogoep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Menu extends JPanel{
   
    private BufferedImage backgroundIntro;
    private BufferedImage backgroundTutorial;
    private BufferedImage background;
    
    private BufferedImage btnIniciar;
    private BufferedImage btnTutorial;
    private BufferedImage btnSair;
    private BufferedImage btnTIniciar;
    private BufferedImage btnTVoltar;
    
    private Map mapa;
    
    int selecaoMenu = 00;
   
    public Menu(){
        
        setDoubleBuffered(true);
        
        try {
            background = ImageIO.read(new File("src/jogoep2/img/space.jpg"));
            backgroundIntro = ImageIO.read(new File("src/jogoep2/img/spaceIntro.jpg"));
            backgroundTutorial = ImageIO.read(new File("src/jogoep2/img/backTutorial.jpg"));
            btnIniciar = ImageIO.read(new File("src/jogoep2/img/iniciar.jpg"));
            btnTutorial = ImageIO.read(new File("src/jogoep2/img/tutorial.jpg"));
            btnSair = ImageIO.read(new File("src/jogoep2/img/sair.jpg"));
            btnTVoltar = ImageIO.read(new File("src/jogoep2/img/voltar.jpg"));
            btnTIniciar = ImageIO.read(new File("src/jogoep2/img/iniciarMT.jpg"));
            
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Handlerclass handler = new Handlerclass();
        this.addMouseListener(handler);
        initComponents();
        mapa = new Map();
        painelJogo.add(mapa);
        mapa.setVisible(false);
               
    }
        
    @Override
    public void paint(Graphics g) {
        
        super.paintComponent(g);
        if(selecaoMenu == 00){
            g.drawImage(backgroundIntro, -15, 0, null);
            g.drawImage(btnIniciar, 145, 250, null);
            g.drawImage(btnTutorial, 175, 310, null);
            g.drawImage(btnSair, 175, 350, null);
        }
        
        if(selecaoMenu == 01){
            g.drawImage(backgroundTutorial, -25, -55, null);
            g.drawImage(btnTVoltar, 10, 450, null);
            g.drawImage(btnTIniciar, 355, 450, null);
        }
        
        if(selecaoMenu == 10){
            g.drawImage(background, 0, 0, null);
            mapa.setVisible(true);
            mapa.setFocusable(true);
            mapa.requestFocus(true);
        }         
    }
    
    private class Handlerclass implements MouseListener{
        
        @Override
        public void mouseClicked(MouseEvent e) {
            
            if(selecaoMenu == 00){
                if ((e.getButton() == 1) && (e.getX() > 145 &&  e.getX() < 347) && (e.getY() > 250 &&  e.getY() < 310) ) {
                    selecaoMenu = 10;        
                    repaint();
                }
            
                if ((e.getButton() == 1) && (e.getX() > 175 &&  e.getX() < 347) && (e.getY() > 310 &&  e.getY() < 348) ) {
                    selecaoMenu = 01;
                    repaint();
                }
            
                if ((e.getButton() == 1) && (e.getX() > 175 &&  e.getX() < 318) && (e.getY() > 350 &&  e.getY() < 388) ) {
                    System.exit(0);
                }          
            }
            
            if(selecaoMenu == 01){
                if ((e.getButton() == 1) && (e.getX() > 10 &&  e.getX() < 153) && (e.getY() > 450 &&  e.getY() < 488) ) {
                    selecaoMenu = 00;
                    repaint();
                }
            
                if ((e.getButton() == 1) && (e.getX() > 355 &&  e.getX() < 498) && (e.getY() > 450 &&  e.getY() < 488) ) {
                    selecaoMenu = 10;
                    repaint();
                }          
            }
            
            if(selecaoMenu == 10){
                if ((e.getButton() == 1) && (e.getX() > 170 &&  e.getX() < 313) && (e.getY() > 220 &&  e.getY() < 258) ) {
                    selecaoMenu = 00;
                    repaint();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            
            
        }

        @Override
        public void mouseReleased(MouseEvent e) {
             
        }

        @Override
        public void mouseEntered(MouseEvent e) {
          
        }

        @Override
        public void mouseExited(MouseEvent e) {
           
        }
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelJogo = new javax.swing.JPanel();

        setBackground(new java.awt.Color(1, 1, 1));
        setFocusable(false);
        setMaximumSize(new java.awt.Dimension(500, 500));
        setPreferredSize(new java.awt.Dimension(500, 500));
        setRequestFocusEnabled(false);
        setVerifyInputWhenFocusTarget(false);

        painelJogo.setBackground(new java.awt.Color(1, 1, 1));
        painelJogo.setMaximumSize(new java.awt.Dimension(500, 500));
        painelJogo.setMinimumSize(new java.awt.Dimension(500, 500));
        painelJogo.setPreferredSize(new java.awt.Dimension(500, 500));
        painelJogo.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelJogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelJogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel painelJogo;
    // End of variables declaration//GEN-END:variables

   
}