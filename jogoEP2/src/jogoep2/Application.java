package jogoep2;

import java.awt.EventQueue;
import javax.swing.JFrame;


public class Application extends JFrame {
     
    Menu menu;
    
    public Application() {
        setSize(Game.getWidth(), Game.getHeight());
        setTitle("Space Combat Game");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null); 
        menu = new Menu();
        add(menu);
        
    }
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }
}
